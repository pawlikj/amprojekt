import React from "react";
import { StyleSheet, Text, View, Pressable } from 'react-native';
import {useState, useEffect} from 'react';
import AddRec from './addRecipe'

export default function AddRecipeScreen({ navigation, route, newData}){
    const [data, setdata] = useState([])
    useEffect(() => {
        const getData = async ()=>{
          const dataFromServer = await fetchData()
          setdata(dataFromServer)
        }
      getData()
    }, [])
    //Fetch data
    const fetchData = async() =>{
      const res = await fetch('http://localhost:5000/data')
      const data = await res.json()
      return data
    }

    const addRecipe = async (data) => {
        const res = await fetch('http://localhost:5000/data',{
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify(data),
        })
        navigation.pop()
    }

    return(
        <View style={styles.container}>
            <AddRec onAdd = {addRecipe}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#C52222',
      alignItems: 'flex-start',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      alignItems: 'left',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 4,
      elevation: 3,
      backgroundColor: '#126641',
    },
    text: {
      fontSize: 16,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'white',
    },
    dataText: {
      fontSize: 16,
      lineHeight: 51,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'white',
      backgroundColor: '#204075',
      lineWidth:'70%',
      borderWidth:2,
      borderRadius: 6,
      textAlign: 'center'
    },
  });
  