import React from "react";
import { Pressable, StyleSheet, Text, View } from 'react-native';

export default function Home({navigation}) {

    const naviAbout = ()=>{
        navigation.push('About')
    }

    const naviLista = ()=>{
        navigation.push('List')
    }

    return(
    <View style={styles.container}>
    <Text style={styles.text}>Home Screen</Text>
    <Pressable style={styles.button} title='Lista dań' onPress={naviLista}>
        <Text style={styles.text}>Lista dań</Text>
    </Pressable>
    <Pressable style={styles.button} title='About' onPress={naviAbout}>
        <Text style={styles.text}>About</Text>
    </Pressable>
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#C52222',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: '#70A5A2',
      },
      text: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      },    
  });
  