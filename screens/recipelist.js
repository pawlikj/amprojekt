import React from "react";
import { StyleSheet, Text, View, Pressable } from 'react-native';
import {useState, useEffect} from 'react';
import Data from "./Data"


export default function List({navigation}) {
    const [data, setdata] = useState([])
    useEffect(() => {
        const getData = async ()=>{
          const dataFromServer = await fetchData()
          setdata(dataFromServer)
        }
      getData()
    }, [])
    //Fetch data
    const fetchData = async() =>{
      const res = await fetch('http://localhost:5000/data')
      const data = await res.json()
      return data
    }

    const fetchDataInstance = async(id) =>{
      console.log("fetching id ",id);
        const res = await fetch("http://localhost:5000/data/{id}")
        const data = await res.json()
        return data
    }


    const onPressHandler = ()=>{
        navigation.pop();
    }
    const onPressNewRecipeHandler = ()=>{
      navigation.navigate('NewRecipe')
    }
    const openRecipe = (id,name,desc,ingredients)=>{
      
      navigation.navigate('Recipe',{id: id, name: name, desc: desc, ingredients: ingredients})
    }

    return(
    <View style={styles.container}>
      
      <Data data={data} onToggle={openRecipe}></Data>

    <Pressable style={styles.button}  title='Dodaj Przepis' onPress={onPressNewRecipeHandler}>
      <Text style={styles.text}>Dodaj Przepis</Text>
    </Pressable>

    <Pressable style={styles.button}  title='Return' onPress={onPressHandler}>
      <Text style={styles.text}>Return</Text>
    </Pressable>

    </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#C52222',
    alignItems: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'left',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#126641',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  dataText: {
    fontSize: 16,
    lineHeight: 51,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
    backgroundColor: '#204075',
    lineWidth:'70%',
    borderWidth:2,
    borderRadius: 6,
    textAlign: 'center'
  },
});
