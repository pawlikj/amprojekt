import {useState} from 'react'

const AddData = ({onAdd})=>{
    const [name, setName] = useState('')
    const [ingredients, setIngredients] = useState('')
    const [description, setDescription] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()
    
        if (!name) {
          alert('Dodaj danie')
          return
        }
    
        onAdd({ name, ingredients, description })
    
        setName('')
        setIngredients('')
        setDescription('')
      }

    return (
        <form className='add-form' onSubmit={onSubmit}>
          <div className='form-control'>
            <label>Nazwa Dania</label>
            <input
              type='text'
              placeholder='Name of the dish'
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className='form-control'>
            <label>Lista składników</label>
            <input
              type='text'
              placeholder='List of ingredients'
              value={ingredients}
              onChange={(e) => setIngredients(e.target.value)}
            />
          </div>
          <div className='form-control'>
            <label>Przepis</label>
            <input
              type='text'
              placeholder='Recipe'
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
    
          <input type='submit' value='Save Dish' className='btn btn-block' />
        </form>
      )
}

export default AddData