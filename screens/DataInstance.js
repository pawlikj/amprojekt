import { Pressable, StyleSheet, View, Text } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler";

const DataInstance = ({ dataInstance, onDelete, onToggle}) => {
  return (
    <View>
      <TouchableOpacity onPress={() => onToggle(dataInstance.id, dataInstance.name, dataInstance.description, dataInstance.ingredients)}>
        <Text style = {styles.text}>{dataInstance.name}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default DataInstance

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#C52222',
    alignItems: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'left',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#126641',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  dataText: {
    fontSize: 16,
    lineHeight: 51,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
    backgroundColor: '#204075',
    lineWidth:'70%',
    borderWidth:2,
    borderRadius: 6,
    textAlign: 'center'
  },
});