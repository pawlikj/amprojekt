import React from "react";
import { Button, StyleSheet, Text, View } from 'react-native';

export default function About({navigation}) {
    const onPressHandler = ()=>{
        navigation.pop();
    }
    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Hello from About</Text>
            <Button title='Return' onPress={onPressHandler}/>
        </View>
    )
}