import React from "react";
import { Button, StyleSheet, Text, View, Pressable } from 'react-native';
import {useState, useEffect, useLayoutEffect} from 'react';
import { TouchableOpacity } from "react-native-gesture-handler";
import { FaCreativeCommonsPd } from "react-icons/fa";

export default function Recipe({navigation, route}) {
  //const [data, setdata] = useState(route.params.data);
  //const [index, setindex] = useState(route.params.index);
  const id = route.params.id;
  const name = route.params.name;
  const desc = route.params.desc;
  const ingredients = route.params.ingredients;
  
    const onPressHandler = ()=>{
      navigation.pop()
    }
    return(
    <View style={styles.container}>
        <Text style={styles.text}>
          {name}
        </Text>
        <Text style={styles.text}>
          Składniki: {ingredients}
        </Text>
        <Text style={styles.text}>
          Przepis: {desc}
        </Text>
    <Pressable style={styles.button}  title='Return' onPress={onPressHandler}>
      <Text style={styles.text}>Return</Text>
    </Pressable>
    </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#C52222',
    alignItems: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'left',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#126641',
  },
  textTitle: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  dataText: {
    fontSize: 16,
    lineHeight: 51,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
    backgroundColor: '#204075',
    lineWidth:'70%',
    borderWidth:2,
    borderRadius: 6,
    textAlign: 'center'
  },
});
