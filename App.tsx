import { StyleSheet} from 'react-native';
import Navigator from './navi/naviStack'

export default function App() {
  return (
    <Navigator />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#B06282',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
