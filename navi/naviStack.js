import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Home from '../screens/home'
import About from '../screens/about'
import List from '../screens/recipelist'
import Recipe from '../screens/showRecipe'
import NewRecipe from '../screens/addRecipeScreen'

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();

function tabNavi() {
    return(
        <Tab.Navigator>
            <Tab.Screen name = "Home" component = {Home} />
            <Tab.Screen name = "List" component = {List} />
        </Tab.Navigator>
    )
}

export default function Navigation() {
    return(
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="Home" component={tabNavi}/>
            <Stack.Screen name="About" component={About}/>
            <Stack.Screen name="List" component={tabNavi}/>
            <Stack.Screen name="Recipe" component={Recipe}/>
            <Stack.Screen name="NewRecipe" component={NewRecipe}/>
        </Stack.Navigator>
    </NavigationContainer>
    )
}